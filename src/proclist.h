#ifndef __PROCLIST_H__
#define __PROCLIST_H__

#include <signal.h>

pid_t getPIDbyname( const char* procname, bool exactlymatched = false, char *fullname = 0 );

#endif /// of __PROCLIST_H__
