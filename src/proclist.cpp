#include "proclist.h"

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdarg>

#include <dirent.h>

#define PROCDIR     "/proc/"

bool isnumberic( const char* cst )
{
    for ( ; *cst; cst++ )
    {
        if ( ( *cst < '0' ) || ( *cst > '9' ) )
            return false;
    }

    return true;
}

pid_t getPIDbyname( const char* procname, bool exactlymatched, char *fullname )
{
    char    cmdlinepath[100] = {0};
    char    nameofproc[300] = {0};
    char*   pcompare = NULL;
    pid_t   curproc = (pid_t) -1;
    struct
    dirent* dire = NULL;
    DIR*    dirproc = NULL;

    dirproc = opendir( PROCDIR );
    if ( dirproc == NULL )
    {
        return curproc;
    }

    while( dire = readdir( dirproc ) )
    {
        if ( dire->d_name != NULL )
        {
            sprintf( cmdlinepath, "%s%s/cmdline", PROCDIR, dire->d_name );
        }

        FILE* cmdfd = fopen( cmdlinepath, "rt" );
        if ( cmdfd != NULL )
        {
            fscanf( cmdfd, "%s", nameofproc );
            fclose( cmdfd );
            char* ppos = strrchr( nameofproc, '/' );
            if ( ppos != NULL )
                pcompare = ppos + 1;
            else
                pcompare = nameofproc;

            if ( exactlymatched == true )
            {
                if ( strcmp( pcompare, procname ) == 0 )
                {
                    if ( fullname != NULL )
                    {
                        strcpy( fullname, pcompare );
                    }

                    curproc = (pid_t) atoi ( dire->d_name );
                    closedir( dirproc );
                    return curproc;
                }
            }
            else
            {
                if ( strstr( pcompare, procname ) != NULL )
                {
                    if ( fullname != NULL )
                    {
                        strcpy( fullname, pcompare );
                    }

                    curproc = (pid_t) atoi ( dire->d_name );
                    closedir( dirproc );
                    return curproc;
                }
            }
        }
    }

    closedir( dirproc );
    return curproc;

}
