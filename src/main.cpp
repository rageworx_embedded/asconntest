#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "proclist.h"

#define DEF_VERSION_S           "1.2.1"
#define DEF_DAEMON_NAME         "asctrld"
#define DEF_LOG_DIR             "/var/asconntest/"
#define DEF_LOG_FNAME           "asctrld_restart_log"
#define DEF_CHECK_PERIOD_SEC    5

int main_loop()
{
    while( true )
    {
        // check process ..
        pid_t pid = getPIDbyname( DEF_DAEMON_NAME, false, NULL );

        if ( pid <= 0 )
        {
            // restart asctrld !
            printf( "restarting daemon = %s...", DEF_DAEMON_NAME );
            system( "killall "DEF_DAEMON_NAME );
            //system( DEF_DAEMON_NAME" start" );

            // Let's make a breath ..
            sleep( 3 );

            system( "/etc/init.d/"DEF_DAEMON_NAME" start" );
            printf( "done.\n" );

            // Wait more ....
            sleep( DEF_CHECK_PERIOD_SEC );
        }

        sleep( DEF_CHECK_PERIOD_SEC );
    }

    return 0;
}

int main(int argc, char** argv)
{
	printf( "asctrld process watching daemon, version %s\n", DEF_VERSION_S );
	printf( "(C)2016 3iware raph.k.\n" );

    int reti = 0;

    if ( fork() == 0 )
    {
        reti = main_loop();
    }
    else
    {
        exit( 0 );
    }

	return reti;
}
