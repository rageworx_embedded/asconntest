# Initializing local compile environment
CHKDIRS="./bin ./bin/Release ./obj ./obj/Release"

for chks in $CHKDIRS
do
	if [ ! -e $chks ]; then
		mkdir $chks
	fi
done

